package sk.tuke.kpi.oop.game.bomb;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Alive;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class TimeBomb extends AbstractActor {
    private final float time;
    private final Animation activatedBomb;
    private final Animation explosion;
    private boolean isActivated;
    private boolean exploded;

    public TimeBomb(float time) {
        this.time = time;
        activatedBomb = new Animation("sprites/bomb_activated.png", 16, 16, 0.1f);
        explosion = new Animation("sprites/small_explosion.png", 16, 16, 0.1f, Animation.PlayMode.ONCE);
        setAnimation(new Animation("sprites/bomb.png", 16, 16));
        isActivated = false;
    }

    public float getTime() {
        return time;
    }

    public boolean isExploded() {
        return exploded;
    }

    public void activate() {
        if (getScene() == null || isActivated) {
            return;
        }
        isActivated = true;
        setAnimation(activatedBomb);
        new ActionSequence<>(
            new Wait<>(time),
            new Invoke<>(() -> {
                setAnimation(explosion);
                exploded = true;
                Ellipse2D.Float ellipse = new Ellipse2D.Float(this.getPosX() + this.getWidth() / 2 - 50, this.getPosY() + this.getHeight() / 2 - 50, 100, 100);
                List<Actor> list = getScene().getActors();
                for (Actor actor : list) {
                    Rectangle2D.Float rect = new Rectangle2D.Float(actor.getPosX(), actor.getPosY(), actor.getWidth(), actor.getHeight());
                    if (actor instanceof Alive && ellipse.intersects(rect)) {
                        ((Alive) actor).getHealth().drain(60);
                    }
                }
            })
        ).scheduleOn(this);
        new When<>(
            action -> explosion.getFrameCount() - 1 == explosion.getCurrentFrameIndex(),
            new Invoke<>(() -> getScene().removeActor(this))
        ).scheduleOn(this);
    }

    public boolean isActivated(){
        return isActivated;
    }
}
