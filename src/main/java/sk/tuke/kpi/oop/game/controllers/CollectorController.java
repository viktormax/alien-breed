package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.actions.Drop;
import sk.tuke.kpi.oop.game.actions.Shift;
import sk.tuke.kpi.oop.game.actions.Take;
import sk.tuke.kpi.oop.game.actions.Use;
import sk.tuke.kpi.oop.game.items.Collectible;
import sk.tuke.kpi.oop.game.items.Usable;

public class CollectorController implements KeyboardListener {
    private final Keeper<Collectible> collector;

    public CollectorController(Keeper<Collectible> collector){
        this.collector = collector;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        switch (key) {
            case ENTER:
                new Take<>(Collectible.class).scheduleOn(collector);
                break;
            case S:
                new Shift().scheduleOn(collector);
                break;
            case BACKSPACE:
                new Drop<Collectible>().scheduleOn(collector);
                break;
            case U:
                useItem();
                break;
            case B:
                useFromContainer();
                break;
            default:
        }
    }

    private void useFromContainer(){
        Disposable disposable = null;
        Collectible item = null;
        if (collector.getContainer().getSize() != 0) {
            item = collector.getContainer().peek();
            if (item instanceof Usable<?>) {
                disposable = new Use<>((Usable<?>)item).scheduleOnIntersectingWith(collector);
            }
        }
        if (disposable != null) {
            collector.getContainer().remove(item);
        }
    }

    private void useItem(){
        Scene scene = collector.getScene();
        if (scene == null) {
            return;
        }
        for (Actor usable : scene.getActors()) {
            if (usable.intersects(collector) && usable instanceof Usable) {
                new Use<>((Usable<?>)usable).scheduleOnIntersectingWith(collector);
            }
        }
    }
}
