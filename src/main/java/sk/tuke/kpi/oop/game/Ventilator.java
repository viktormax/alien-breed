package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;

public class Ventilator extends AbstractActor implements Repairable {
    public static final Topic<Ventilator> VENTILATOR_REPAIRED = new Topic<>("ventilator repaired", Ventilator.class);
    private final Animation coolerAnimation;
    private boolean isRepaird;

    public Ventilator() {
        coolerAnimation = new Animation("sprites/ventilator.png", 32, 32, 0.1f);
        setAnimation(coolerAnimation);
        coolerAnimation.stop();
        isRepaird = false;
    }

    @Override
    public boolean repair() {
        if (isRepaird || getScene() == null) {
            return false;
        }
        coolerAnimation.play();
        getScene().getMessageBus().publish(VENTILATOR_REPAIRED, this);
        isRepaird = true;
        return true;
    }
}
