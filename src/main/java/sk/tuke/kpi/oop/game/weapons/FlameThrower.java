package sk.tuke.kpi.oop.game.weapons;

import sk.tuke.kpi.gamelib.graphics.Animation;

public class FlameThrower extends Firearm {

    public FlameThrower(int ammo, int maxAmmo) {
        super(ammo, maxAmmo);
        setAnimation(new Animation("sprites/weapon.png", 64, 32));
    }

    public FlameThrower(int ammo) {
        super(ammo);
        setAnimation(new Animation("sprites/weapon.png", 64, 32));
    }

    @Override
    protected Fireable createBullet() {
        return new Flame();
    }

    @Override
    public int getSpeed() {
        return 0;
    }

}
