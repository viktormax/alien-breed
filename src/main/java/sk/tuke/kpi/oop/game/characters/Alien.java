package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;

import java.util.List;

public class Alien extends AbstractActor implements Movable, Enemy, Alive, Prototype {
    private final int speed;
    private Health health;
    private Animation alienAnimation;
    private Direction direction;
    private final Behaviour<? super Alien> behaviour;
    private final int initHealth;

    public Alien(int initHealth, Behaviour<? super Alien> behaviour) {
        alienAnimation = new Animation("sprites/alien.png", 32, 32, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        setAnimation(alienAnimation);
        alienAnimation.pause();
        this.behaviour = behaviour;
        this.initHealth = initHealth;
        health = new Health(initHealth);
        speed = 1;
    }

    public Alien() {
        alienAnimation = new Animation("sprites/alien.png", 32, 32, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        setAnimation(alienAnimation);
        alienAnimation.pause();
        behaviour = null;
        initHealth = 20;
        health = new Health(initHealth);
        speed = 1;
    }

    public Prototype createClone() {
        return new Alien(this.initHealth, this.behaviour);
    }

    public void updateAnimation() {
        alienAnimation = getAnimation();
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        if (behaviour != null) {
            behaviour.setUp(this);
        }

        new When<>(
            action -> getHealth().getValue() == 0,
            new Invoke<>(() -> {scene.removeActor(this);
                Ripley.getInstance().addScore(initHealth);
            })
        ).scheduleOn(this);

        new Loop<>(new Invoke<>(() -> {
            List<Actor> actors = scene.getActors();
            for (Actor actor : actors) {
                if (actor instanceof Alive && !(actor instanceof Enemy) && this.intersects(actor)) {
                    ((Alive) actor).getHealth().drain(1);
                }
            }
        })).scheduleOn(this);
    }

    @Override
    public void startedMoving(Direction direction) {
        this.direction = direction;
        alienAnimation.setRotation(direction.getAngle());
        alienAnimation.play();
    }

    @Override
    public void stoppedMoving() {
        alienAnimation.stop();
    }

}
