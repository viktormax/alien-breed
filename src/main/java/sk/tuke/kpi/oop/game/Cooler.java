package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Cooler extends AbstractActor implements Switchable{
    private boolean isOn;
    private final Animation coolerAnimation;
    private Reactor reactor;

    public Cooler(Reactor reactor) {
        this.reactor = reactor;
        coolerAnimation = new Animation("sprites/fan.png", 32,32, 0.2f);
        isOn = false;
        coolerAnimation.pause();
        setAnimation(coolerAnimation);
    }

    public void setReactor(Reactor reactor) {
        this.reactor = reactor;
    }

    public Reactor getReactor() {
        return reactor;
    }

    @Override
    public boolean isOn() {
        return isOn;
    }

    @Override
    public void turnOn() {
        isOn = true;
        coolerAnimation.play();
    }

    @Override
    public void turnOff() {
        isOn = false;
        coolerAnimation.pause();
    }

    public void coolReactor(){
        if (reactor == null) {
            return;
        }
        if (isOn) {
            reactor.decreaseTemperature(1);
        }
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(this::coolReactor)).scheduleOn(this);
    }
}
