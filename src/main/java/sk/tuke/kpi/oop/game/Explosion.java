package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Explosion extends AbstractActor {
    private final Animation explosionAnimation;

    public Explosion(Size size) {
        if (size == Size.SMALL) {
            explosionAnimation = new Animation("sprites/small_explosion.png", 16, 16, 0.05f, Animation.PlayMode.ONCE);
        } else {
            explosionAnimation = new Animation("sprites/large_explosion.png", 32, 32, 0.05f, Animation.PlayMode.ONCE);
        }
        setAnimation(explosionAnimation);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new When<>(
            action -> explosionAnimation.getFrameCount() - 1 == explosionAnimation.getCurrentFrameIndex(),
            new Invoke<>(() -> scene.removeActor(this))
        ).scheduleOn(this);
    }

    public enum Size {
        SMALL,
        BIG
    }
}
