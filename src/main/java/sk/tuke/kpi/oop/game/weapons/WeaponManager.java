package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class WeaponManager{
    private final List<Firearm> list = new Stack<>();

    public WeaponManager(Firearm firearm){
        add(firearm);
    }

    public List<Firearm> getContent() {
        return List.copyOf(list);
    }

    public void add(@NotNull Firearm actor) {
        list.add(actor);
    }

    public Firearm peek() {
        if (!list.isEmpty()) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public void shift() {
        Collections.rotate(list, 1);
    }

    public Iterator<Firearm> iterator() {
        return list.iterator();
    }
}
