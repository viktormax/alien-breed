package sk.tuke.kpi.oop.game.characters;

import java.util.ArrayList;
import java.util.List;

public class Health {
    private final List<ExhaustionEffect> effects = new ArrayList<>();
    private int healthValue;
    private boolean stop;
    private final int maxHealth;

    public Health(int healthValue, int maxHealth) {
        this.healthValue = healthValue;
        this.maxHealth = maxHealth;
        stop = false;
    }

    public Health(int healthValue) {
        this.healthValue = healthValue;
        this.maxHealth = healthValue;
    }

    public int getValue() {
        return healthValue;
    }

    public void restore() {
        healthValue = maxHealth;
    }

    public void refill(int amount) {
        healthValue += amount;
        healthValue = healthValue > maxHealth ? maxHealth : healthValue;
    }

    public void drain(int amount) {
        healthValue -= amount;
        if (healthValue <= 0) {
            exhaust();
        }
    }

    public void exhaust() {
        healthValue = 0;
        if (!stop) {
            stop = true;
            effects.forEach(ExhaustionEffect::apply);
        }
    }

    public void onExhaustion(ExhaustionEffect effect) {
        effects.add(effect);
    }

    @FunctionalInterface
    public interface ExhaustionEffect {
        void apply();
    }
}
