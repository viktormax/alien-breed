package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.ActorContainer;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Keeper;

import java.util.List;

public class Take<A extends Actor> extends AbstractAction<Keeper<A>> {
    private final Class<A> takableActorsClass;

    public Take(Class<A> takableActorsClass){
        this.takableActorsClass = takableActorsClass;
    }

    @Override
    public void execute(float deltaTime) {
        Keeper<A> keeper = getActor();
        if (keeper == null) {
            setDone(true);
            return;
        }
        Scene scene = keeper.getScene();
        if (scene == null) {
            setDone(true);
            return;
        }
        ActorContainer<A> container = keeper.getContainer();
        List<Actor> list = scene.getActors();
        for(Actor actor : list) {
            if(keeper.intersects(actor) && takableActorsClass.isInstance(actor)) {
                try {
                    container.add(takableActorsClass.cast(actor));
                    scene.removeActor(actor);
                } catch (Exception exception) {
                    scene.getGame().getOverlay().drawText(exception.getMessage(), 10, 32).showFor(2);
                }
                break;
            }
        }
        setDone(true);
    }
}
