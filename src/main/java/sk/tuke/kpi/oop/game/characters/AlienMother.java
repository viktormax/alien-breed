package sk.tuke.kpi.oop.game.characters;

import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;

public class AlienMother extends Alien {
    private final int initHealth;
    private Behaviour<? super Alien> behaviour;

    public AlienMother(int healthValue, Behaviour<? super Alien> behaviour) {
        super(healthValue, behaviour);
        this.initHealth = healthValue;
        this.behaviour = behaviour;
        setAnimation(new Animation("sprites/mother.png", 112, 162, 0.2f, Animation.PlayMode.LOOP_PINGPONG));
        updateAnimation();
    }

    public AlienMother() {
        super();
        setAnimation(new Animation("sprites/mother.png", 112, 162, 0.2f, Animation.PlayMode.LOOP_PINGPONG));
        updateAnimation();
        initHealth = 200;
        setHealth(new Health(initHealth));
    }

    public Prototype createClone() {
        return new AlienMother(this.initHealth, this.behaviour);
    }
}
