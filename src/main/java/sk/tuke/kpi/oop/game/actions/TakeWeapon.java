package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.weapons.Firearm;
import sk.tuke.kpi.oop.game.weapons.WeaponManager;

import java.util.List;

public class TakeWeapon extends AbstractAction<Armed> {

    public TakeWeapon(){ }

    @Override
    public void execute(float deltaTime) {
        Armed armed = getActor();
        if (armed == null) {
            setDone(true);
            return;
        }
        Scene scene = armed.getScene();
        if (scene == null) {
            setDone(true);
            return;
        }
        WeaponManager container = armed.getWeaponManager();
        List<Actor> list = scene.getActors();
        for(Actor actor : list) {
            if(armed.intersects(actor) && actor instanceof Firearm) {
                container.add((Firearm)actor);
                scene.removeActor(actor);
            }
        }
        setDone(true);
    }
}
