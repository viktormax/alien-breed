package sk.tuke.kpi.oop.game.behaviours;

import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;

public class RandomlyMoving implements Behaviour<Movable> {
    private Direction direction;

    public RandomlyMoving(){}

    @Override
    public void setUp(Movable actor) {
        if (actor == null) {
            return;
        }
        getRandomDirection();
        Move<Movable> m = new Move<>(direction);
        m.scheduleOn(actor);
        new Loop<>(
            new ActionSequence<>(
                new Invoke<>(this::getRandomDirection),
                new Invoke<>(() -> m.setDirection(direction)),
                new Invoke<>(() -> actor.startedMoving(direction)),
                new Wait<>(0.3f),
                new Invoke<>(() -> {
                    if (Math.round(Math.random() * 6) == 5) {
                        m.setDirection(Direction.NONE);
                        actor.stoppedMoving();
                    }

                }),
                new Wait<>(0.8f)
            )
        ).scheduleOn(actor);
    }

    private void getRandomDirection(){
        int random = (int)(Math.random() * 9);
        Direction[] directions = Direction.values();
        direction = directions[random];
    }
}
