package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Ripley;

public class Helicopter extends AbstractActor {
    private Ripley player;

    public Helicopter() {
        Animation heliAnimation = new Animation("sprites/heli.png", 64, 64, 0.04f);
        setAnimation(heliAnimation);
    }

    public void searchAndDestroy() {
        if (getScene() == null) {
            return;
        }
        new Loop<>(new Invoke<>(this::attack)).scheduleOn(this);
        player = (Ripley) getScene().getFirstActorByName("Ellen");
    }

    private void attack() {
        int x = (player.getPosX() - this.getPosX() - player.getWidth() / 2) < 0 ? this.getPosX() - 1 : this.getPosX() + 1;
        int y = (player.getPosY() - this.getPosY() - player.getHeight() / 2) < 0 ? this.getPosY() - 1 : this.getPosY() + 1;
        this.setPosition(x, y);
        if (player.intersects(this)) {
            player.getHealth().drain(1);
        }
    }
}
