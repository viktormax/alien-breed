package sk.tuke.kpi.oop.game.behaviours;

import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;
import sk.tuke.kpi.oop.game.characters.Ripley;

public class Persecution implements Behaviour<Movable> {

    public Persecution() {}

    @Override
    public void setUp(Movable actor) {
        if (actor == null) {
            return;
        }
        Ripley ripley = Ripley.getInstance();
        Move<Movable> m = new Move<>(Direction.NONE);
        m.scheduleOn(actor);
        new Loop<>(
            new Invoke<>(() -> {
                Direction newDirection;
                int dx = ripley.getPosX() - actor.getPosX();
                int dy = ripley.getPosY() - actor.getPosY();
                Direction direction1 = dx < 0 ? Direction.WEST : dx == 0? Direction.NONE : Direction.EAST;
                Direction direction2 = dy < 0 ? Direction.SOUTH : dy == 0? Direction.NONE : Direction.NORTH;
                newDirection = direction1.combine(direction2);
                m.setDirection(newDirection);
                actor.startedMoving(direction1.combine(newDirection));
            })
        ).scheduleOn(actor);
    }

}
