package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.items.Usable;

public class Use<A extends Actor> extends AbstractAction<A> {
    private final Usable<A> usable;

    public Use(Usable<A> usable){
        this.usable = usable;
    }

    @Override
    public void execute(float deltaTime) {
        A actor = getActor();
        usable.useWith(actor);

        setDone(true);
    }

    public Disposable scheduleOnIntersectingWith(Actor mediatingActor) {
        Scene scene = mediatingActor.getScene();
        if (scene == null) {
            return null;
        }
        Class<A> actorClass = usable.getUsingActorClass();

        return scene.getActors().stream()
            .filter(mediatingActor::intersects)
            .filter(actorClass::isInstance)
            .map(actorClass::cast)
            .findFirst()
            .map(this::scheduleOn)
            .orElse(null);
    }
}
