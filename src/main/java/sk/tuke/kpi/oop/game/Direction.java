package sk.tuke.kpi.oop.game;

public enum Direction {
    NORTH (0, 1),
    NORTHWEST(-1, 1),
    WEST  (-1, 0),
    SOUTHWEST(-1, -1),
    SOUTH (0, -1),
    SOUTHEAST(1, -1),
    EAST  (1, 0),
    NORTHEAST(1, 1),
    NONE  (0, 0);

    private final int dx;
    private final int dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public int getDx(){
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public Direction combine(Direction other) {
        if (other == null) {
            return this;
        }
        Direction direction = Direction.NONE;
        Direction[] directions = Direction.values();
        int newDx = this.dx + other.getDx();
        int newDy = this.dy + other.getDy();
        newDx = Math.max(newDx, -1);
        newDx = Math.min(newDx, 1);
        newDy = Math.max(newDy, -1);
        newDy = Math.min(newDy, 1);
        for (Direction dir : directions) {
            if (dir.getDx() == newDx && dir.getDy() == newDy) {
                direction = dir;
                break;
            }
        }
        return direction;
    }

    public float getAngle() {
        Direction[] directions = Direction.values();
        float angle = 0;
        for (Direction dir : directions) {
            if (dir == this) {
                break;
            }
            angle += 45;
        }
        return angle;
    }

    public static Direction fromAngle(float angle) {
        float newAngle = angle;
        Direction[] directions = Direction.values();
        for (Direction dir : directions) {
            if (newAngle < 1) {
                return dir;
            }
            newAngle -= 45;
        }
        return Direction.NONE;
    }
}
