package sk.tuke.kpi.oop.game.scenarios;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.*;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.*;
import sk.tuke.kpi.oop.game.behaviours.Observing;
import sk.tuke.kpi.oop.game.behaviours.RandomlyMoving;
import sk.tuke.kpi.oop.game.behaviours.ReleaseAlien;
import sk.tuke.kpi.oop.game.bomb.BombSensitive;
import sk.tuke.kpi.oop.game.bomb.ChainBomb;
import sk.tuke.kpi.oop.game.bomb.RipleySensitive;
import sk.tuke.kpi.oop.game.characters.Alien;
import sk.tuke.kpi.oop.game.characters.Ripley;
import sk.tuke.kpi.oop.game.controllers.CollectorController;
import sk.tuke.kpi.oop.game.controllers.MovableController;
import sk.tuke.kpi.oop.game.controllers.RipleyController;
import sk.tuke.kpi.oop.game.controllers.ShooterController;
import sk.tuke.kpi.oop.game.items.*;
import sk.tuke.kpi.oop.game.behaviours.Persecution;
import sk.tuke.kpi.oop.game.music.ProxyRecord;
import sk.tuke.kpi.oop.game.music.RealRecord;
import sk.tuke.kpi.oop.game.music.Sound;
import sk.tuke.kpi.oop.game.openables.Door;
import sk.tuke.kpi.oop.game.openables.LockedDoor;
import sk.tuke.kpi.oop.game.openables.RiseDoor;
import sk.tuke.kpi.oop.game.weapons.FlameThrower;
import sk.tuke.kpi.oop.game.weapons.Mortar;

import java.util.List;

public class MyScenario implements SceneListener {

    private Animation levelFailed;
    private Animation levelCompleted;
    private boolean win;

    public void sceneInitialized(@NotNull Scene scene){
        levelFailed = new Animation("sprites/popup_level_failed.png", 288,128);
        levelCompleted = new Animation("sprites/popup_level_done.png", 288,128);
        scene.getGame().pushActorContainer(Ripley.getInstance().getContainer());
        scene.follow(Ripley.getInstance());

        //BACKGROUND("music/background.wav")
        //FIRE("music/fire.wav")
        RealRecord.volume = RealRecord.Volume.LOW;
        Sound background = new ProxyRecord("music/background.wav");
        background.playLoop();

        Input input = scene.getInput();
        Disposable movableDisposable = input.registerListener(new MovableController(Ripley.getInstance()));
        Disposable collectorDisposable = input.registerListener(new CollectorController(Ripley.getInstance()));
        Disposable shooterController = input.registerListener(new ShooterController(Ripley.getInstance()));
        Disposable ripleyController = input.registerListener(new RipleyController(Ripley.getInstance()));

        scene.getMessageBus().subscribe(Ripley.RIPLEY_DIED, actor ->
            new ActionSequence<>(
                new Invoke<>(collectorDisposable::dispose),
                new Invoke<>(movableDisposable::dispose),
                new Invoke<>(shooterController::dispose),
                new Invoke<>(background::stop),
                new Invoke<>(ripleyController::dispose)
            ).scheduleOn(Ripley.getInstance())
        );

        scene.getMessageBus().subscribe(Door.DOOR_OPENED, actor -> {
            if (actor.getName().equals("door room3")) {
                List<Actor> actors = scene.getActors();
                for (Actor a : actors) {
                    if (a.getName().equals("lamp")) {
                        ((Light) a).setPowered(true);
                        ((Light) a).turnOn();
                    }
                }
            }
        });

        scene.getMessageBus().subscribe(Reactor.REACTOR_ON, actor -> {
            Transporter t = (Transporter)scene.getFirstActorByName("Transporter");
            new Loop<>(
                new Invoke<>(() -> {
                    scene.getGame().getOverlay().drawText("Reactor temperature: " + actor.getTemperature(), 500, scene.getGame().getWindowSetup().getHeight() - 30);
                    assert t != null;
                    if (Ripley.getInstance().intersects(t)) {
                        win = true;
                        scene.getOverlay().drawAnimation(
                            levelCompleted,
                            Ripley.getInstance().getPosX() - 180,
                            Ripley.getInstance().getPosY() - 90
                        );
                    }
                })
            ).scheduleOn(scene);
        });

        scene.getMessageBus().subscribe(Reactor.REACTOR_EXPLODED, actor -> {
            if (!win) {
                Ripley.getInstance().getHealth().exhaust();
            }
        });

        Cooler cooler = (Cooler) scene.getFirstActorByName("Cooler");
        if (cooler != null) {
            cooler.setReactor((Reactor) scene.getFirstActorByName("Reactor"));
        }
    }

    public static class Factory implements ActorFactory {

        @Nullable
        @Override
        public Actor create(@Nullable String type, @Nullable String name) {
            if (name == null) {
                return null;
            }
            switch (name) {
                case "h door":
                    return new LockedDoor(Door.Orientation.HORIZONTAL);
                case "key":
                    return new AccessCard();
                case "f door":
                    return new RiseDoor(name);
                case "heli":
                    return new Helicopter();
                case "transport":
                    return new Transporter(48, 48);
                case "flame thrower":
                    return new FlameThrower(80);
                case "mortar":
                    return new Mortar(40);
                case "locker north":
                    return new Locker(Locker.Orientation.NORTH);
                case "computer":
                    return new Computer();
                case "reactor":
                    return new Reactor();
                case "wrench":
                    return new Wrench();
                case "fire":
                    return new FireExtinguisher();
                case "chain bomb":
                    return new ChainBomb(1f, new BombSensitive());
                case "player":
                    return Ripley.getInstance();
                case "ammo":
                    return new Ammo();
                case "energy":
                    return new Energy();
                case "random alien":
                    return new Alien(20, new RandomlyMoving());
                case "alien":
                    return new Alien();
                case "sensitive bomb":
                    return new ChainBomb(1f, new RipleySensitive());
                case "generator":
                    return new Generator();
                case "v door":
                    return new LockedDoor(Door.Orientation.VERTICAL);
                case "cooler":
                    return new Cooler(null);
                default:
                    Actor actor = room1(name);
                    if (actor == null) {
                        actor = room2(name);
                    }
                    if (actor == null) {
                        actor = room3(name);
                    }
                    if (actor == null) {
                        actor = room4(name);
                    }
                    if (actor == null) {
                        actor = room5(name);
                    }
                    return actor;
            }
        }

        private Actor room1(String name){
            if (name.equals("smart alien room1")) {
                return new Alien(20,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room1"),
                        new Persecution()
                    ));
            }
            if (name.equals("door room1")) {
                return new LockedDoor(name, Door.Orientation.VERTICAL);
            }
            return null;
        }

        private Actor room2(String name) {
            if (name.equals("smart alien room2")) {
                return new Alien(20,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room2"),
                        new Persecution()
                    ));
            }
            if (name.equals("door room2")) {
                return new RiseDoor(name);
            }
            return null;
        }

        private Actor room3(String name) {
            if (name.equals("smart alien room3")) {
                return new Alien(20,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room3"),
                        new Persecution()
                    ));
            }
            if (name.equals("door room3")) {
                return new LockedDoor(name, Door.Orientation.HORIZONTAL);
            }
            if (name.equals("alienteleport")) {
                return new AlienTeleport(
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room3"),
                        new ReleaseAlien()
                    ));
            }
            if (name.equals("lamp")) {
                return new Light("lamp");
            }
            return null;
        }

        private Actor room4(String name) {
            if (name.equals("smart alien room4")) {
                return new Alien(20,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room4") || actor.getName().equals("fdoor room4"),
                        new Persecution()
                    ));
            }
            if (name.equals("door room4")) {
                return new LockedDoor(name, Door.Orientation.HORIZONTAL);
            }
            if (name.equals("fdoor room4")) {
                return new RiseDoor(name);
            }
            if (name.equals("alienteleport room4")) {
                return new AlienTeleport(
                    new Observing<>(
                        Reactor.REACTOR_ON,
                        actor -> true,
                        new ReleaseAlien()
                    ));
            }
            return null;
        }

        private Actor room5(String name) {
            if (name.equals("smart alien room5")) {
                return new Alien(20,
                    new Observing<>(
                        Door.DOOR_OPENED,
                        actor -> actor.getName().equals("door room5"),
                        new Persecution()
                    ));
            }
            if (name.equals("door room5")) {
                return new LockedDoor(name, Door.Orientation.VERTICAL);
            }
            return null;
        }
    }


    @Override
    public void sceneUpdating(@NotNull Scene scene) {
        Ripley ripley = Ripley.getInstance();
        ripley.printRipleyState();
        if (ripley.getHealth().getValue() == 0) {
            scene.getOverlay().drawAnimation(
                levelFailed,
                ripley.getPosX() - 180,
                ripley.getPosY() - 90
            );
        }
    }

}
