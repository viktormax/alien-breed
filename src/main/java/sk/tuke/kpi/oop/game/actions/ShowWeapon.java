package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.weapons.Firearm;

import java.util.Iterator;

public class ShowWeapon extends AbstractAction<Armed> {


    @Override
    public void execute(float deltaTime) {
        Armed armed = getActor();
        if (armed == null) {
            return;
        }
        if (armed.getScene() == null) {
            return;
        }
        Iterator<Firearm> iter = armed.getWeaponManager().iterator();
        int i = 1;
        armed.getScene().getGame().getOverlay().drawText("Weapon:", 20,
            armed.getScene().getGame().getWindowSetup().getHeight() / 2 + 30);
        while(iter.hasNext()){
            Firearm firearm = iter.next();
            armed.getScene().getGame().getOverlay().drawText(i + ". " + firearm.getName() + " " + firearm.getAmmo(), 20,
                armed.getScene().getGame().getWindowSetup().getHeight() / 2 + 30 - 20 * i);
            i++;
        }
    }
}
