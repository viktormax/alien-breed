package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.actions.ChangeWeapon;
import sk.tuke.kpi.oop.game.actions.Fire;
import sk.tuke.kpi.oop.game.actions.ShowWeapon;
import sk.tuke.kpi.oop.game.actions.TakeWeapon;
import sk.tuke.kpi.oop.game.characters.Armed;

public class ShooterController implements KeyboardListener {
    private final Armed armed;
    private Disposable show;


    public ShooterController(Armed armed) {
        this.armed = armed;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (Input.Key.SPACE == key) {
            new Fire<>().scheduleOn(armed);
        }
        if (Input.Key.ENTER == key) {
            new TakeWeapon().scheduleOn(armed);
        }
        if (Input.Key.W == key) {
            new ChangeWeapon().scheduleOn(armed);
        }
        if (Input.Key.P == key) {
            show = new ShowWeapon().scheduleOn(armed);
        }
    }

    @Override
    public void keyReleased(@NotNull Input.Key key) {
        if (Input.Key.P == key) {
            show.dispose();
        }
    }

}
