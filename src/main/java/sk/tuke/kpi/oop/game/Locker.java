package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Ripley;
import sk.tuke.kpi.oop.game.items.Hammer;
import sk.tuke.kpi.oop.game.items.Usable;

public class Locker extends AbstractActor implements Usable<Ripley> {
    private boolean isUsed;

    public Locker(Orientation orientation) {
        Animation lockerAnimation = new Animation("sprites/locker.png", 16, 16);
        setAnimation(lockerAnimation);
        if (orientation == Orientation.NORTH) {
            lockerAnimation.setRotation(270);
        } else if (orientation == Orientation.EAST) {
            lockerAnimation.setRotation(180);
        } else if (orientation == Orientation.SOUTH) {
            lockerAnimation.setRotation(90);
        }
        isUsed = false;
    }

    @Override
    public void useWith(Ripley actor) {
        if (actor == null || isUsed) {
            return;
        }
        isUsed = true;
        if (actor.getScene() == null) {
            return;
        }
        actor.getScene().addActor(new Hammer(), this.getPosX(), this.getPosY());
    }

    @Override
    public Class<Ripley> getUsingActorClass() {
        return Ripley.class;
    }

    public enum Orientation {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }
}
