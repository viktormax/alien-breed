package sk.tuke.kpi.oop.game.music;

public interface Sound {

    void play();
    void stop();
    void playLoop();
}
