package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.framework.actions.Loop;

public class DefectiveLight extends Light implements Repairable {
    private Disposable disposable;
    private boolean isRepaird;

    public DefectiveLight(String name) {
        super(name);
    }

    private void act() {
        int random = (int)(Math.random() * 20);
        if (random == 1) {
            if (isOn()) {
                turnOff();
            } else {
                turnOn();
            }
        }
    }

    private void setRepaird(boolean bool) {
        isRepaird = bool;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        disposable = new Loop<>(new Invoke<>(this::act)).scheduleOn(this);
    }

    @Override
    public boolean repair() {
        if (isRepaird) {
            return false;
        }
        setRepaird(true);
        disposable = new ActionSequence<>(
            new Invoke<>(disposable::dispose),
            new Wait<>(10),
            new Invoke<>(() -> setRepaird(false)),
            new Loop<>(new Invoke<>(this::act))
        ).scheduleOn(this);
        return true;
    }
}
