package sk.tuke.kpi.oop.game.music;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.net.URL;

public class RealRecord implements Sound {
    private Clip clip;

    public static Volume volume = Volume.LOW;

    public enum Volume {
        MUTE, LOW, MEDIUM, HIGH
    }

    public RealRecord(String name) {
        try {
            URL url = this.getClass().getClassLoader().getResource(name);
            if (url != null) {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
                clip = AudioSystem.getClip();
                clip.open(audioInputStream);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void play() {
        if (volume != Volume.MUTE) {
            if (clip.isRunning()) {
                clip.stop();
            }
            clip.setFramePosition(0);
            clip.start();
        }
    }

    @Override
    public void stop() {
        if (clip.isRunning()) {
            clip.stop();
        }
    }

    @Override
    public void playLoop() {
        if (volume != Volume.MUTE) {
            if (clip.isRunning())
                clip.stop();
            clip.loop(100);
            clip.start();
        }
    }
}
