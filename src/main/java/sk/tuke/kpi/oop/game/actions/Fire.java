package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Direction;
//import sk.tuke.kpi.oop.game.Sound;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.weapons.Fireable;

public class Fire<A extends Armed> extends AbstractAction<A> {

    public Fire(){}

    @Override
    public void execute(float deltaTime) {
        Armed armed = getActor();
        if (armed == null) {
            setDone(true);
            return;
        }
        Scene scene = armed.getScene();
        if (scene == null) {
            setDone(true);
            return;
        }
        Fireable bullet = armed.getFirearm().fire();
        if (bullet != null) {
            //Sound.FIRE.play();
            Direction direction = Direction.fromAngle(armed.getAnimation().getRotation());
            bullet.startedMoving(direction);
            scene.addActor(bullet, armed.getPosX() + 8, armed.getPosY() + 8);
            Move<Fireable> moveAction = new Move<>(direction);
            moveAction.scheduleOn(bullet);
        }
        setDone(true);
    }
}
