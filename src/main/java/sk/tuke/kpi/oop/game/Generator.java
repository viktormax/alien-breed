package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Alive;

import java.util.List;

public class Generator extends AbstractActor {

    public Generator() {
       setAnimation(new Animation("sprites/generator.png", 32, 16, 0.1f));
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(() -> {
            List<Actor> actors = scene.getActors();
            for (Actor actor : actors) {
                if (this.intersects(actor) && actor instanceof Alive) {
                    ((Alive) actor).getHealth().drain(1);
                }
            }
        })).scheduleOn(this);
    }
}
