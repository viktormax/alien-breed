package sk.tuke.kpi.oop.game.items;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.ActorContainer;

import java.util.*;

public class Backpack implements ActorContainer<Collectible> {
    private final String name;
    private final int capacity;
    private final List<Collectible> list = new Stack<>();

    public Backpack(String name, int capacity){
        this.name = name;
        this.capacity = capacity;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @NotNull
    @Override
    public List<Collectible> getContent() {
        return List.copyOf(list);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public void add(@NotNull Collectible actor) {
        if (list.size() == capacity) {
            throw new IllegalStateException(String.format("%s is full", getName()));
        }
        list.add(actor);
    }

    @Nullable
    @Override
    public Collectible peek() {
        if (!list.isEmpty()) {
            return list.get(list.size() - 1);
        } else {
            return null;
        }
    }

    @Override
    public void remove(@NotNull Collectible actor) {
        list.remove(actor);
    }

    @Override
    public void shift() {
        Collections.rotate(list, 1);
    }

    @NotNull
    @Override
    public Iterator<Collectible> iterator() {
        return list.iterator();
    }
}
