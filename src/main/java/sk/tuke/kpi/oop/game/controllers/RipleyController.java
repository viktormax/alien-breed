package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.actions.*;
import sk.tuke.kpi.oop.game.characters.Ripley;

public class RipleyController implements KeyboardListener {
    private final Ripley ripley;

    public RipleyController(Ripley ripley) {
        this.ripley = ripley;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (Input.Key.O == key) {
            new Switch<>().scheduleOn(ripley);
        }
    }
}
