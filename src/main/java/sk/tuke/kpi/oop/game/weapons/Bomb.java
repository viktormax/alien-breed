package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.Explosion;

import java.util.List;

public class Bomb extends AbstractActor implements Fireable {
    private final int speed;
    private final Animation bombAnimation;

    public Bomb() {
        bombAnimation = new Animation("sprites/weapon_bomb.png", 16, 16);
        setAnimation(bombAnimation);
        speed = 3;
    }

    @Override
    public int getSpeed() {
        return speed;
    }


    @Override
    public void startedMoving(Direction direction){
        bombAnimation.setRotation(direction.getAngle());
        bombAnimation.play();
    }

    @Override
    public void stoppedMoving(){
        bombAnimation.stop();
    }

    public void collidedWithWall() {
        if (getScene() == null) {
            return;
        }
        getScene().removeActor(this);
        List<Actor> actors = getScene().getActors();
        Explosion explosion = new Explosion(Explosion.Size.BIG);
        getScene().addActor(explosion, getPosX() - 8, getPosY() - 8);
        inRange(actors, explosion);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(() -> {
            List<Actor> actors = scene.getActors();
            for (Actor actor : actors) {
                if (!(actor instanceof Armed) && this.intersects(actor) && actor instanceof Alive) {
                    scene.removeActor(this);
                    Explosion explosion = new Explosion(Explosion.Size.BIG);
                    scene.addActor(explosion, getPosX() - 8, getPosY() - 8);
                    inRange(actors, explosion);
                }
            }
        })).scheduleOn(this);
    }

    private void inRange(List<Actor> actors, Explosion explosion) {
        for (Actor actor : actors) {
            if (explosion.intersects(actor) && actor instanceof Alive) {
                ((Alive) actor).getHealth().drain(20);
            }
        }
    }
}

