package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Disposable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.characters.Ripley;

public class Transporter extends AbstractActor {
    private final int x, y;

    public Transporter(int x, int y) {
        Animation heliAnimation = new Animation("sprites/heli.png", 64, 64, 0.04f);
        setAnimation(heliAnimation);
        this.x = x;
        this.y = y;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        Disposable disposable = new Loop<>(new Invoke<>(() -> {
            int n_x = (x - this.getPosX()) < 0 ? this.getPosX() - 1 : (x - this.getPosX()) == 0? x : this.getPosX() + 1;
            int n_y = (y - this.getPosY()) < 0 ? this.getPosY() - 1 : (y - this.getPosY()) == 0? y : this.getPosY() + 1;
            this.setPosition(n_x, n_y);
            Ripley.getInstance().setPosition(n_x + 16, n_y + 16);
        })).scheduleOn(this);

        new When<>(
            action -> x == this.getPosX() && y == this.getPosY(),
            new Invoke<>(disposable::dispose)
        ).scheduleOn(this);
    }
}
