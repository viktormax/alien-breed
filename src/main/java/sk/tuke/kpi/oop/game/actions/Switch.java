package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Switchable;
import sk.tuke.kpi.oop.game.characters.Ripley;

import java.util.List;

public class Switch<A extends Switchable> extends AbstractAction<Ripley> {

    public Switch() {}

    @Override
    public void execute(float deltaTime) {
        Ripley ripley = getActor();
        if (ripley == null) {
            setDone(true);
            return;
        }
        Scene scene = ripley.getScene();
        if (scene == null) {
            setDone(true);
            return;
        }
        List<Actor> list = scene.getActors();
        for(Actor actor : list) {
            if(ripley.intersects(actor) && actor instanceof Switchable) {
                ((Switchable)actor).turnOn();
            }
        }
        setDone(true);
    }
}
