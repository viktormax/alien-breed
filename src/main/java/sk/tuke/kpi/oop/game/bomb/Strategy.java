package sk.tuke.kpi.oop.game.bomb;

import sk.tuke.kpi.gamelib.Actor;

public interface Strategy<A extends Actor> {

    void execute(A actor);
}
