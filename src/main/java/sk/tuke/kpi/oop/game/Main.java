package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.*;
import sk.tuke.kpi.oop.game.scenarios.MyScenario;

public class Main {

    public static void main(String[] argv) {
        WindowSetup windowSetup = new WindowSetup("Project Ellen",970,500);
        Game game = new GameApplication(windowSetup);

        Scene scene = new World("world", "maps/map.tmx", new MyScenario.Factory());
        game.addScene(scene);

        scene.addListener(new MyScenario());

        game.start();

        scene.getInput().onKeyPressed(key -> {
            if (key == Input.Key.ESCAPE) {
                scene.getGame().stop();
            }
        });
    }
}
