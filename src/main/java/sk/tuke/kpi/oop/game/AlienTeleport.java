package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.behaviours.Behaviour;

public class AlienTeleport extends AbstractActor {
    private final Behaviour<? super AlienTeleport> behaviour;

    public AlienTeleport(Behaviour<? super AlienTeleport> behaviour) {
        this.behaviour = behaviour;
        setAnimation(new Animation("sprites/lift.png", 48, 48));
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        if (behaviour != null) {
            behaviour.setUp(this);
        }
    }
}
