package sk.tuke.kpi.oop.game.characters;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.ActorContainer;
import sk.tuke.kpi.gamelib.Game;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.graphics.Overlay;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Keeper;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.items.Backpack;
import sk.tuke.kpi.oop.game.items.Collectible;
import sk.tuke.kpi.oop.game.weapons.Firearm;
import sk.tuke.kpi.oop.game.weapons.Gun;
import sk.tuke.kpi.oop.game.weapons.WeaponManager;

public class Ripley extends AbstractActor implements Alive, Movable, Armed, Keeper<Collectible> {
    public static  Ripley instance;
    private final Animation player;
    private final Animation deadPlayer;
    private final Backpack backpack;
    private final Health health;
    private int speed;
    private Direction direction;
    private final WeaponManager weaponContainer;
    private Firearm firearm;
    private int score;


    public Ripley() {
        super("Ellen");
        player = new Animation("sprites/player.png", 32, 32, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        deadPlayer = new Animation("sprites/player_die.png", 32,32,0.1f, Animation.PlayMode.ONCE);
        setAnimation(player);
        backpack = new Backpack("Ripley's backpack", 10);
        health = new Health(100);
        weaponContainer = new WeaponManager(new Gun(50, 50));
        firearm = weaponContainer.peek();
        player.stop();
        score = 0;
        speed = 2;
    }

    public void addScore(int score){
        this.score += score;
    }

    public static Ripley getInstance(){
        if (instance == null) {
            instance = new Ripley();
        }
        return instance;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public ActorContainer<Collectible> getContainer() {
        return backpack;
    }

    public WeaponManager getWeaponManager() {
        return weaponContainer;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public int getSpeed(){
        return speed;
    }

    @Override
    public void startedMoving(Direction direction){
        this.direction = direction;
        player.setRotation(direction.getAngle());
        player.play();
    }

    @Override
    public void stoppedMoving(){
        player.stop();
    }

    public void printRipleyState(){
        if (getScene() == null) {
            return;
        }
        Game game = getScene().getGame();
        Overlay overlay = game.getOverlay();
        overlay.drawText(
            "| Energy: " + Integer.toString(health.getValue()) +
                " | Ammo:" + firearm.getAmmo() + " | Score:" + score, 100, game.getWindowSetup().getHeight() - 30
        );
    }

    public static final Topic<Ripley> RIPLEY_DIED = new Topic<>("Ripley died", Ripley.class);

    @Override
    public Health getHealth() {
        return health;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        health.onExhaustion(() -> {
            scene.cancelActions(this);
            scene.getMessageBus().publish(RIPLEY_DIED, this);
            setAnimation(deadPlayer);
        });
    }

    @Override
    public Firearm getFirearm() {
        return firearm;
    }

    @Override
    public void setFirearm(Firearm weapon) {
        firearm = weapon;
    }

}
