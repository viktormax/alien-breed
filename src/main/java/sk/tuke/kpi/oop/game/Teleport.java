package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.When;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.Player;
import sk.tuke.kpi.gamelib.framework.actions.Loop;

import sk.tuke.kpi.gamelib.graphics.Animation;

public class Teleport extends AbstractActor {
    private Teleport destinationTeleport;
    private boolean stop;

    public Teleport(Teleport teleport) {
        setAnimation(new Animation("sprites/lift.png", 48, 48));
        stop = false;
        if (teleport != this) {
            this.destinationTeleport = teleport;
        }
    }

    public void setStop(){
        stop = true;
    }

    public void setDestination(Teleport destinationTeleport){
        if (destinationTeleport != this) {
            this.destinationTeleport = destinationTeleport;
        }
    }

    public Teleport getDestination() {
        return destinationTeleport;
    }

    public void teleportPlayer(Player player){
        if (player == null) {
            return;
        }
        int x = this.getPosX() + this.getWidth() / 2 - player.getWidth() / 2;
        int y = this.getPosY() + this.getHeight() / 2 - player.getHeight() / 2;
        player.setPosition(x, y);
        this.setStop();
    }

    private boolean isInTeleport(Player player) {
        int x_t_c = this.getPosX() + this.getWidth() / 2;
        int y_t_c = this.getPosY() + this.getHeight() / 2;
        int x_p_c = player.getPosX() + player.getWidth() / 2;
        int y_p_c = player.getPosY() + player.getHeight() / 2;
        if (stop && ((Math.abs(x_t_c - x_p_c) > this.getWidth() / 2) || (Math.abs(y_t_c - y_p_c) > this.getHeight() / 2))) {
            stop = !stop;
        }
        return ((Math.abs(x_t_c - x_p_c) <= this.getWidth() / 2) && (Math.abs(y_t_c - y_p_c) <= this.getHeight() / 2) && !stop);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        Player player = (Player) scene.getFirstActorByName("Player");
        if (player == null) {
            return;
        }
        new Loop<>(
            new When<>(
                action -> isInTeleport(player) && destinationTeleport != null,
                new Invoke<>(() -> destinationTeleport.teleportPlayer(player))
            )
        ).scheduleOn(this);
    }
}
