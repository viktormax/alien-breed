package sk.tuke.kpi.oop.game.openables;

import sk.tuke.kpi.gamelib.Actor;

public class LockedDoor extends Door {
    private boolean isLocked;

    public LockedDoor(Orientation orientation){
        super(orientation);
        isLocked = true;
    }

    public LockedDoor(String name, Orientation orientation) {
        super(name, orientation);
        isLocked = true;
    }

    @Override
    public void useWith(Actor actor) {
        if (super.isOpen()) {
            lock();
        } else {
            unlock();
        }
    }

    public void setLocked(boolean isLocked){
        this.isLocked = isLocked;
    }

    public void lock(){
        if(super.isOpen()) {
            super.close();
        }
    }

    public void unlock(){
        if(!isLocked && !super.isOpen()) {
            super.open();
        }
    }

    public boolean isLocked(){
        return isLocked;
    }
}
