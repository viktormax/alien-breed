package sk.tuke.kpi.oop.game.music;

public class ProxyRecord implements Sound {
    private final String name;
    private RealRecord realRecord;

    public ProxyRecord(String name) {
        this.name = name;
        this.realRecord = null;
    }

    @Override
    public void play() {
        if (realRecord == null) {
            realRecord = new RealRecord(this.name);
        }
        realRecord.play();
    }

    @Override
    public void stop() {
        if (realRecord != null) {
            realRecord.stop();
        }
    }

    @Override
    public void playLoop() {
        if (realRecord == null) {
            realRecord = new RealRecord(this.name);
        }
        realRecord.playLoop();
    }
}
