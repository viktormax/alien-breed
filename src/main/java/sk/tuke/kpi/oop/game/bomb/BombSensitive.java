package sk.tuke.kpi.oop.game.bomb;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class BombSensitive implements Strategy<ChainBomb> {



    @Override
    public void execute(ChainBomb actor) {
        Scene scene = actor.getScene();
        if (scene == null) {
            return;
        }
        new Loop<>(
            new Invoke<>(() -> {
                List<Actor> list = scene.getActors();
                for (Actor a : list) {
                    if (a instanceof ChainBomb) {
                        Ellipse2D.Float ellipse = new Ellipse2D.Float(actor.getPosX() + actor.getWidth() / 2 - 50, actor.getPosY() + actor.getHeight() / 2 - 50, 100, 100);
                        Rectangle2D.Float rect = new Rectangle2D.Float(a.getPosX(), a.getPosY(), a.getWidth(), a.getHeight());
                        if (ellipse.intersects(rect) && ((ChainBomb) a).isExploded() ) {
                            actor.activate();
                        }
                    }
                }
            })
        ).scheduleOn(actor);
    }
}
