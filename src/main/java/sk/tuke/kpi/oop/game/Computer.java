package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Computer extends AbstractActor implements EnergyConsumer{
    private final Animation computerAnimation;
    private boolean flow;

    public Computer() {
        computerAnimation = new Animation("sprites/computer.png", 80, 48, 0.2f);
        setAnimation(computerAnimation);
        computerAnimation.stop();
        flow = false;
    }

    public int add(int a, int b) {
        return flow ? a + b : 0;
    }

    public float add(float a, float b) {
        return flow ? a + b : 0;
    }

    public int sub(int a, int b) {
        return flow ? a - b : 0;
    }

    public float sub(float a, float b) {
        return flow ? a - b : 0;
    }

    @Override
    public void setPowered(boolean flow) {
        this.flow = flow;
        if (flow) {
            computerAnimation.play();
        } else {
            computerAnimation.stop();
        }
    }
}
