package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.ActorContainer;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.Keeper;

public class Drop<A extends Actor> extends AbstractAction<Keeper<A>> {

    public Drop(){}

    @Override
    public void execute(float deltaTime) {
        Keeper<A> keeper = getActor();
        if (keeper == null) {
            setDone(true);
            return;
        }
        Scene scene = keeper.getScene();
        if (scene == null) {
            setDone(true);
            return;
        }
        ActorContainer<A> container = keeper.getContainer();
        A item = container.peek();
        if (item != null) {
            keeper.getContainer().remove(item);
            scene.addActor(item,
                keeper.getPosX() + keeper.getWidth() / 2 - item.getWidth() / 2,
                keeper.getPosY() + keeper.getHeight() / 2 - item.getHeight() / 2
            );
        }
        setDone(true);
    }
}
