package sk.tuke.kpi.oop.game.openables;

import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.map.MapTile;

public class RiseDoor extends LockedDoor {

    public RiseDoor(String name) {
        super(name, Orientation.VERTICAL);
        setAnimation(new Animation("sprites/fdoor.png", 16, 48, 0.1f));
    }

    @Override
    public void open() {
        super.open();
        Scene scene = getScene();
        if (scene == null) {
            return;
        }
        getAnimation().setPlayMode(Animation.PlayMode.ONCE);
        getAnimation().play();
        MapTile tile = scene.getMap().getTile(getPosX() / 16, getPosY() / 16 + 2);
        tile.setType(MapTile.Type.CLEAR);
    }

    @Override
    public void close() {
        super.close();
        Scene scene = getScene();
        if (scene == null) {
            return;
        }
        getAnimation().setPlayMode(Animation.PlayMode.ONCE_REVERSED);
        getAnimation().play();
        MapTile tile = scene.getMap().getTile(getPosX() / 16, getPosY() / 16 + 2);
        tile.setType(MapTile.Type.WALL);
    }
}
