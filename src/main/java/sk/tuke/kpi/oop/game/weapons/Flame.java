package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.characters.Armed;

import java.util.List;

public class Flame extends AbstractActor implements Fireable {
    private final int speed;
    private final Animation flameAnimation;

    public Flame() {
        flameAnimation = new Animation("sprites/small_explosion.png", 16, 16, 0.1f);
        setAnimation(flameAnimation);
        speed = 4;
    }

    @Override
    public int getSpeed() {
        return speed;
    }


    @Override
    public void startedMoving(Direction direction){
        flameAnimation.setRotation(direction.getAngle());
        flameAnimation.play();
    }

    @Override
    public void stoppedMoving(){
        flameAnimation.stop();
    }

    public void collidedWithWall() {
        if (getScene() == null) {
            return;
        }
        getScene().removeActor(this);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(() -> {
            List<Actor> actors = scene.getActors();
            for (Actor actor : actors) {
                if (!(actor instanceof Armed) && this.intersects(actor) && actor instanceof Alive) {
                    ((Alive)actor).getHealth().drain(1);
                }
            }
            if (flameAnimation.getFrameCount() - 1 == flameAnimation.getCurrentFrameIndex()) {
                scene.removeActor(this);
            }
        })).scheduleOn(this);
    }


}


