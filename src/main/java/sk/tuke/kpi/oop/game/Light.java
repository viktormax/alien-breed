package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;

public class Light extends AbstractActor implements Switchable, EnergyConsumer {
    private final Animation lightOnAnimation;
    private final Animation lightOffAnimation;
    private boolean electricityFlow;
    private boolean isOn;
    private final String name;

    public Light(String name) {
        this.name = name;
        lightOnAnimation = new Animation("sprites/light_on.png", 16, 16);
        lightOffAnimation = new Animation("sprites/light_off.png", 16, 16);
        setAnimation(lightOffAnimation);
        electricityFlow = false;
        isOn = false;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void turnOn() {
        isOn = true;
        if (electricityFlow) {
            setAnimation(lightOnAnimation);
        }
    }

    @Override
    public void turnOff() {
        isOn = false;
        setAnimation(lightOffAnimation);
    }

    public void toggle(){
        isOn = !isOn;
    }

    @Override
    public boolean isOn() {
        return isOn;
    }

    @Override
    public void setPowered(boolean flow) {
        this.electricityFlow = flow;
        if (electricityFlow && isOn) {
            setAnimation(lightOnAnimation);
        } else {
            setAnimation(lightOffAnimation);
        }
    }
}
