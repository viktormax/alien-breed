package sk.tuke.kpi.oop.game.behaviours;

import sk.tuke.kpi.gamelib.actions.ActionSequence;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.actions.Wait;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.oop.game.AlienTeleport;
import sk.tuke.kpi.oop.game.characters.Alien;


public class ReleaseAlien implements Behaviour<AlienTeleport> {
    public ReleaseAlien() {}

    @Override
    public void setUp(AlienTeleport actor) {
        if (actor == null) {
            return;
        }
        if (actor.getScene() == null) {
            return;
        }
        Alien alien = new Alien(20, new Persecution());
        new Loop<>(
            new ActionSequence<>(
                new Wait<>(8),
                new Invoke<>(() ->
                    actor.getScene().addActor(alien.createClone(), actor.getPosX(), actor.getPosY())
                )
            )
        ).scheduleOn(actor);
    }

}
