package sk.tuke.kpi.oop.game.openables;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.map.MapTile;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.items.Usable;

public class Door extends AbstractActor implements Openable, Usable<Actor> {
    public static final Topic<Door> DOOR_OPENED = Topic.create("door opened", Door.class);
    public static final Topic<Door> DOOR_CLOSED = Topic.create("door closed", Door.class);
    private final Orientation orientation;
    private boolean isOpen;
    private Scene scene;
    private MapTile tile1;
    private MapTile tile2;


    public Door(Orientation orientation) {
        this.orientation = orientation;
        if (orientation == Orientation.VERTICAL) {
            setAnimation(new Animation("sprites/vdoor.png", 16, 32, 0.1f));
        } else if (orientation == Orientation.HORIZONTAL) {
            setAnimation(new Animation("sprites/hdoor.png", 32, 16, 0.1f));
        }
        getAnimation().stop();
    }

    public Door(String name, Orientation orientation) {
        super(name);
        this.orientation = orientation;
        if (orientation == Orientation.VERTICAL) {
            setAnimation(new Animation("sprites/vdoor.png", 16, 32, 0.1f));
        } else {
            setAnimation(new Animation("sprites/hdoor.png", 32, 16, 0.1f));
        }
        getAnimation().stop();
    }

    @Override
    public void open() {
        getAnimation().setPlayMode(Animation.PlayMode.ONCE);
        getAnimation().play();
        getTile();
        tile1.setType(MapTile.Type.CLEAR);
        tile2.setType(MapTile.Type.CLEAR);
        isOpen = true;
        scene.getMessageBus().publish(DOOR_OPENED, this);
    }

    @Override
    public void close() {
        getAnimation().setPlayMode(Animation.PlayMode.ONCE_REVERSED);
        getAnimation().play();
        getTile();
        tile1.setType(MapTile.Type.WALL);
        tile2.setType(MapTile.Type.WALL);
        isOpen = false;
        scene.getMessageBus().publish(DOOR_CLOSED, this);
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    private void getTile() {
        tile1 = scene.getMap().getTile(getPosX() / 16, getPosY() / 16);
        if (orientation == Orientation.VERTICAL) {
            tile2 = scene.getMap().getTile(getPosX() / 16, getPosY() / 16 + 1);
        } else if (orientation == Orientation.HORIZONTAL) {
            tile2 = scene.getMap().getTile(getPosX() / 16 + 1, getPosY() / 16);
        }
    }

    @Override
    public void useWith(Actor actor) {
        if (isOpen) {
            close();
        } else {
            open();
        }
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        this.scene = scene;
        this.close();
    }

    public Class<Actor> getUsingActorClass() {
        return Actor.class;
    }

    public enum Orientation {
        HORIZONTAL,
        VERTICAL
    }

}
