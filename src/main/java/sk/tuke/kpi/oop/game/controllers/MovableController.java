package sk.tuke.kpi.oop.game.controllers;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Input;
import sk.tuke.kpi.gamelib.KeyboardListener;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;
import sk.tuke.kpi.oop.game.actions.Move;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MovableController implements KeyboardListener {
    private final Movable actor;
    private final Set<Direction> directions = new HashSet<>();
    private Move<Movable> move;
    private final Map<Input.Key, Direction> keyDirectionMap = Map.ofEntries(
        Map.entry(Input.Key.UP, Direction.NORTH),
        Map.entry(Input.Key.DOWN, Direction.SOUTH),
        Map.entry(Input.Key.LEFT, Direction.WEST),
        Map.entry(Input.Key.RIGHT, Direction.EAST)
    );

    public MovableController(Movable actor){
        this.actor = actor;
    }

    @Override
    public void keyPressed(@NotNull Input.Key key) {
        if (!keyDirectionMap.containsKey(key)) {
            return;
        }
        directions.add(keyDirectionMap.get(key));
        refresh();
    }

    @Override
    public void keyReleased(@NotNull Input.Key key) {
        if (!keyDirectionMap.containsKey(key)) {
            return;
        }
        directions.remove(keyDirectionMap.get(key));
        refresh();
    }

    private void refresh(){
        if (move != null) {
            move.stop();
        }
        int dirSize = directions.size();
        if (dirSize > 0) {
            Direction[] dir = directions.toArray(new Direction[dirSize]);
            for (int i = 0; i < dirSize - 1; i++) {
                move = new Move<>(dir[i].combine(dir[i + 1]));
            }
            if (dirSize == 1) {
                move = new Move<>(dir[0]);
            }
            move.scheduleOn(actor);
        }
    }
}
