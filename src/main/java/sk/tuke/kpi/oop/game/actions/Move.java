package sk.tuke.kpi.oop.game.actions;

import org.jetbrains.annotations.Nullable;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Action;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.Movable;

public class Move<A extends Movable> implements Action<A>{
    private A actor;
    private boolean isBegan;
    private boolean isDone;
    private Direction direction;
    private final float duration;
    private float runTime;

    public Move(Direction direction, float duration) {
        this.duration = duration;
        this.direction = direction;
        isDone = false;
        isBegan = false;
        runTime = 0;
    }

    public Move(Direction direction){
        this.duration = -1;
        this.direction = direction;
        isDone = false;
        isBegan = false;
    }

    @Nullable
    @Override
    public A getActor() {
        return actor;
    }

    @Override
    public void setActor(@Nullable A actor) {
        this.actor = actor;
    }

    @Override
    public boolean isDone() {
        return isDone;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public Direction getDirection(){ return direction; }

    @Override
    public void reset() {
        isDone = false;
        isBegan = false;
        runTime = 0;
    }

    public void stop() {
        if (actor == null) {
            return;
        }
        actor.stoppedMoving();
        isDone = true;
    }

    @Override
    public void execute(float deltaTime) {
        Scene scene = actor.getScene();
        if (scene == null || isDone || direction == Direction.NONE) {
            return;
        }
        runTime += deltaTime;
        if (!isBegan) {
            actor.startedMoving(direction);
            isBegan = true;
        }
        if (duration != -1 && (Math.abs(runTime - duration) < 1e-5 || runTime > duration)) {
            stop();
        }
        int newY = actor.getPosY() + actor.getSpeed() * direction.getDy();
        actor.setPosition(actor.getPosX(), newY);
        int newX = actor.getPosX() + actor.getSpeed() * direction.getDx();
        actor.setPosition(newX, actor.getPosY());
        if (scene.getMap().intersectsWithWall(actor)) {
            onWall(scene);
            actor.collidedWithWall();
        }
    }

    private void onWall(Scene scene) {
        int oldY = actor.getPosY();
        int oldX = actor.getPosX();
        if (scene.getMap().intersectsWithWall(actor)) {
            actor.setPosition(oldX - actor.getSpeed() * direction.getDx(), oldY);
            if (scene.getMap().intersectsWithWall(actor)) {
                actor.setPosition(oldX, oldY);
            }
        }
        if (scene.getMap().intersectsWithWall(actor)) {
            actor.setPosition(oldX, oldY - actor.getSpeed() * direction.getDy());
            if (scene.getMap().intersectsWithWall(actor)) {
                actor.setPosition(oldX, oldY);
            }
        }
        if (scene.getMap().intersectsWithWall(actor)) {
            actor.setPosition(oldX - actor.getSpeed() * direction.getDx(), oldY - actor.getSpeed() * direction.getDy());
        }
    }
}
