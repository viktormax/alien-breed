package sk.tuke.kpi.oop.game.characters;

import sk.tuke.kpi.gamelib.Actor;

public interface Prototype extends Actor {

    Prototype createClone();
}
