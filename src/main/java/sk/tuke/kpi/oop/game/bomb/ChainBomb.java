package sk.tuke.kpi.oop.game.bomb;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;



public class ChainBomb extends TimeBomb {
    private final Strategy<? super ChainBomb> strategy;

    public ChainBomb(float time, Strategy<? super ChainBomb> strategy) {
        super(time);
        this.strategy = strategy;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        if (strategy != null) {
            strategy.execute(this);
        }

    }
}


