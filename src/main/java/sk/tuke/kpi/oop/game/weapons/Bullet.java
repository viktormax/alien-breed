package sk.tuke.kpi.oop.game.weapons;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Actor;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.actions.Invoke;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.framework.actions.Loop;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.oop.game.Direction;
import sk.tuke.kpi.oop.game.characters.Alive;
import sk.tuke.kpi.oop.game.characters.Armed;
import sk.tuke.kpi.oop.game.Explosion;

import java.util.List;

public class Bullet extends AbstractActor implements Fireable {
    private final int speed;
    private final Animation bulletAnimation;

    public Bullet() {
        bulletAnimation = new Animation("sprites/bullet.png", 16, 16);
        setAnimation(bulletAnimation);
        speed = 4;
    }

    @Override
    public int getSpeed() {
        return speed;
    }


    @Override
    public void startedMoving(Direction direction){
        bulletAnimation.setRotation(direction.getAngle());
        bulletAnimation.play();
    }

    @Override
    public void stoppedMoving(){
        bulletAnimation.stop();
    }

    public void collidedWithWall() {
        if (getScene() == null) {
            return;
        }
        getScene().addActor(new Explosion(Explosion.Size.SMALL), getPosX(), getPosY());
        getScene().removeActor(this);
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new Loop<>(new Invoke<>(() -> {
            List<Actor> actors = scene.getActors();
            for (Actor actor : actors) {
                if (!(actor instanceof Armed) && this.intersects(actor) && actor instanceof Alive) {
                    ((Alive) actor).getHealth().drain(10);
                    scene.removeActor(this);
                    scene.addActor(new Explosion(Explosion.Size.SMALL), getPosX(), getPosY());
                }
            }
        })).scheduleOn(this);
    }
}
