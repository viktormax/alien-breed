package sk.tuke.kpi.oop.game;

import org.jetbrains.annotations.NotNull;
import sk.tuke.kpi.gamelib.Scene;
import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.messages.Topic;
import sk.tuke.kpi.oop.game.actions.PerpetualReactorHeating;

import java.util.HashSet;
import java.util.Set;

public class Reactor extends AbstractActor implements Switchable, Repairable {
    private final Animation normalAnimation;
    private final Animation hotAnimation;
    private final Animation brokenAnimation;
    private final Animation offAnimation;
    private final Animation reactorExtinguished;
    private final Set<EnergyConsumer> devices;
    private boolean isRunning;
    private int temperature;
    private int damage;


    public Reactor() {
        temperature = 0;
        damage = 0;
        isRunning = false;
        reactorExtinguished = new Animation("sprites/reactor_extinguished.png", 80, 80);
        offAnimation = new Animation("sprites/reactor.png", 80, 80);
        normalAnimation = new Animation("sprites/reactor_on.png", 80, 80, 0.1f, Animation.PlayMode.LOOP_PINGPONG);
        hotAnimation = new Animation("sprites/reactor_hot.png", 80, 80, 0.05f, Animation.PlayMode.LOOP_PINGPONG);
        brokenAnimation = new Animation("sprites/reactor_broken.png", 80, 80, 0.1f);
        setAnimation(offAnimation);
        devices = new HashSet<>();
    }

    @Override
    public boolean isOn(){
        return this.isRunning;
    }

    public int getDamage() {
        return damage;
    }

    public int getTemperature() {
        return temperature;
    }

    @Override
    public void turnOn() {
        if (damage == 100) {
            return;
        }
        for (EnergyConsumer device : devices) {
            device.setPowered(true);
        }
        isRunning = true;
        updateAnimation();
        if (getScene() != null) {
            getScene().getMessageBus().publish(REACTOR_ON, this);
        }
    }

    @Override
    public void turnOff() {
        isRunning = false;
        for (EnergyConsumer device : devices) {
            device.setPowered(false);
        }
        updateAnimation();
    }

    public void increaseTemperature(int increment) {
        if (damage == 100 || increment <= 0 || !isRunning) {
            return;
        }
        if (damage < 33) {
            temperature += increment;
        } else if (damage <= 66) {
            temperature += (int)Math.ceil(increment * 1.5);
        } else {
            temperature += (int)Math.ceil(increment * 2);
        }
        if (temperature >= 2000) {
            if (temperature >= 6000) {
                damage = 100;
                turnOff();
            } else {
                int newDamage = ((temperature - 2000) * 100) / 4000;
                if (newDamage > damage) {
                    damage = newDamage;
                }
            }
        }
        updateAnimation();
    }

    public void decreaseTemperature(int decrement) {
        if (decrement <= 0 || !isRunning) {
            return;
        }
        if (damage < 50) {
            temperature -= decrement;
        } else if (damage < 100) {
            temperature -= decrement * 0.5;
        }
        temperature = temperature < 0 ? 0 : temperature;
        updateAnimation();
    }

    private void updateAnimation() {
        if (!isRunning) {
            if (damage == 100 && temperature >= 6000) {
                if (getScene() != null) {
                    getScene().getMessageBus().publish(REACTOR_EXPLODED, this);
                }
                setAnimation(brokenAnimation);
            } else {
                setAnimation(offAnimation);
            }
        } else if (temperature <= 4000) {
            setAnimation(normalAnimation);
        } else {
            setAnimation(hotAnimation);
        }
    }

    @Override
    public boolean repair() {
        if (damage == 100 || damage == 0) {
            return false;
        }
        damage -= 50;
        int newDamage = damage;
        if (damage < 0) {
            damage = 0;
        }
        this.temperature = (newDamage * 4000) / 100 + 2000;
        updateAnimation();
        return true;
    }

    public void addDevice(EnergyConsumer device) {
        this.devices.add(device);
        if (isRunning && damage != 100) {
            device.setPowered(true);
        }
    }

    public void removeDevice(EnergyConsumer device) {
        device.setPowered(false);
        this.devices.remove(device);
    }

    public boolean extinguish() {
        if (this.damage < 100) {
            return false;
        }
        temperature -= 4000;
        temperature = temperature < 0 ? 0 : temperature;
        setAnimation(reactorExtinguished);
        return true;
    }

    @Override
    public void addedToScene(@NotNull Scene scene) {
        super.addedToScene(scene);
        new PerpetualReactorHeating(2).scheduleOn(this);
    }

    public static final Topic<Reactor> REACTOR_ON = Topic.create("reactor switched", Reactor.class);
    public static final Topic<Reactor> REACTOR_EXPLODED = Topic.create("reactor exploded", Reactor.class);
}
