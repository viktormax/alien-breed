package sk.tuke.kpi.oop.game;

import sk.tuke.kpi.gamelib.framework.AbstractActor;
import sk.tuke.kpi.gamelib.graphics.Animation;
import sk.tuke.kpi.gamelib.graphics.Color;

public class PowerSwitch extends AbstractActor {
    private Switchable device;

    public PowerSwitch(Switchable device) {
        if (device == null) {
            return;
        }
        this.device = device;
        setAnimation(new Animation("sprites/switch.png", 16, 16));
    }

    public Switchable getDevice(){
        return this.device;
    }

    public void switchOn() {
        if (device == null) {
            return;
        }
        getAnimation().setTint(Color.WHITE);
        this.device.turnOn();
    }

    public void switchOff() {
        if (device == null) {
            return;
        }
        getAnimation().setTint(Color.GRAY);
        this.device.turnOff();
    }
}
