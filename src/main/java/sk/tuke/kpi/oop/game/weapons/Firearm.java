package sk.tuke.kpi.oop.game.weapons;

import sk.tuke.kpi.gamelib.framework.AbstractActor;

public abstract class Firearm extends AbstractActor implements Fireable {
    private int ammo;
    private final int maxAmmo;

    public Firearm(int ammo, int maxAmmo) {
        this.ammo = ammo;
        this.maxAmmo = maxAmmo;
    }

    public Firearm(int ammo) {
        this.ammo = ammo;
        this.maxAmmo = ammo;
    }

    public int getAmmo() {
        return ammo;
    }

    public void reload(int newAmmo) {
        ammo += newAmmo;
        ammo = ammo > maxAmmo ? maxAmmo : ammo;
    }

    public Fireable fire() {
        if (ammo > 0) {
            ammo--;
            return createBullet();
        }
        return null;
    }

    protected abstract Fireable createBullet();
}
