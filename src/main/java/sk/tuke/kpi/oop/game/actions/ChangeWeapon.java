package sk.tuke.kpi.oop.game.actions;

import sk.tuke.kpi.gamelib.framework.actions.AbstractAction;
import sk.tuke.kpi.oop.game.characters.Armed;

public class ChangeWeapon extends AbstractAction<Armed>{

    public ChangeWeapon() {}

    @Override
    public void execute(float deltaTime) {
        setDone(true);
        if (getActor() == null) {
            return;
        }
        getActor().getWeaponManager().shift();
        getActor().setFirearm(getActor().getWeaponManager().peek());
    }
}
