package sk.tuke.kpi.oop.game.weapons;

import sk.tuke.kpi.gamelib.graphics.Animation;

public class Mortar extends Firearm {

    public Mortar(int ammo, int maxAmmo) {
        super(ammo, maxAmmo);
        setAnimation(new Animation("sprites/weapon.png", 64, 32));
    }

    public Mortar(int ammo) {
        super(ammo);
        setAnimation(new Animation("sprites/weapon.png", 64, 32));
    }

    @Override
    protected Fireable createBullet() {
        return new Bomb();
    }

    @Override
    public int getSpeed() {
        return 0;
    }

}
